<jsp:include page="header.jsp"/>

<%@page import="java.util.List" %>
<%@page import="cl.Inacap.Odiseo.DAO.LibroDAO" %> 
<%@page import="cl.Inacap.Odiseo.DTO.Libro"%>

    <section class="container-fluid">

        <div class="row">
            <div class="col-4 text-center">
                <img src="${selectLibro.portadaLibro }" alt="">
            </div>
            <div class="col-6 text-center">

                <div class="col-10 text-center">
                    <div class="col-12 col-centered text-center"><h1>${selectLibro.nombreLibro }</h1></div>
                    <div class="col-12 col-centered text-center"><h2>${selectLibro.autorLibro }</h2></div>
                    <div class="col-12 col-centered text-center">Cantidad de Paginas: ${selectLibro.cantPaginas }</div>
                    <div class="col-12 col-centered text-center">Calificaion: ${selectLibro.estrellas }</div>
                    <div class="col-12 col-centered text-center">Categoria: ${selectLibro.categoria }</div>
                    <div class="col-md-12" style="background-color: black;"><hr></div>
                    <div class="col-12 col-centered text-center">
                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Alias similique dignissimos animi quae laudantium eos optio et consequatur atque reprehenderit quo, ratione, amet perferendis minima veniam reiciendis officia tempore rem?
                    </div>
                 </div>
            </div>
        </div>
    </section>
    
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</html>