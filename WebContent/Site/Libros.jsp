<jsp:include page="header.jsp"/>
    
<%@page import="java.util.List" %>
<%@page import="cl.Inacap.Odiseo.DAO.LibroDAO" %> 
<%@page import="cl.Inacap.Odiseo.DTO.Libro"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link rel="stylesheet" href="Site/css/font-awesome.min.css">
<link rel="stylesheet" href="Site/css/jquery.dataTables.min.css">
    
        <div class="container" style="margin-top: 20px;">
        <div class="row">
            <div class="col-11 col-centered text-center">
            	<div class="row">
            		<div class="col-md-12">
            			 <h1>Libros</h1>
            		</div>
            		<div class="col-md-12">
                        <table class="table table-bordered table-striped" id="tabla-trabajo">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Autor</th>
                                    <th>N� Paginas</th>
                                    <th>Categoria</th>
                                </tr>
                            </thead>
                            <tbody>                     
                            	<c:forEach items="${Libros }"  var="l" varStatus="recorrido"> 
                            		<tr>
                            			<td>${l.nombreLibro }</td>
                            			<td>${l.autorLibro }</td>
                            			<td>${l.cantPaginas }</td>
                            			<td>${l.categoria } </td>
                                    </tr>
                            	</c:forEach>
                            </tbody>
                        </table>
            		</div>
            	</div>
            </div>
        </div>
    </div>
    
</body>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script src="Site/js/jquery.dataTables.js"></script>

<script>
    $(document).ready(function (){
        $("#tabla-trabajo").DataTable();
    })
</script>

</html>