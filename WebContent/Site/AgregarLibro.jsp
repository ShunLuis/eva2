<jsp:include page="header.jsp"/>

    <section>

        <form action="${IndexLibro>=0?'EditarLibroControllers.do' : 'AgregarLibroControllers.do'}" method="POST" name="formulario" id="formularioID">

            <input type="hidden" value="${IndexLibro}" name="index">

            <div class="col-4 text-center col-centered border">
                <hr>
                <h1 style="color: black">AGREGAR LIBRO</h1>
                <hr>
                <label for="Nombre" style="color: black">Nombre</label>
                <input type="text" class="form-control" name="Nombre" id="Nombre" onchange="validarNombre(this.value)" value="${selectLibro.nombreLibro }">
                <hr>
                <label for="Autor" style="color: black">Autor</label>
                <input type="text" class="form-control" name="Autor" id="Autor" onchange="validarAutor(this.value)" value="${selectLibro.autorLibro }">
                <hr>
                <label for="CantidadDePaginas" style="color: black">Cantidad de Paginas</label>
                <input type="text" class="form-control" name="CantidadDePaginas" id="CantidadDePaginas" onchange="validarCantidadDePaginas(this.value)" value="${selectLibro.cantPaginas }">
                <hr>
                <label for="Destacado" style="color: black">Destacado</label>
                <select class="form-control" name="Destacado" id="Destacado">
                    <option value="Si">Si</option>
                    <option value="No">No</option>
                </select>
                <hr>
                <label for="Portada" style="color: black">Portada</label>
                <input type="text" class="form-control" name="Portada" id="Portada" onchange="validarPortada(this.value)" value="${selectLibro.portadaLibro }">
                <hr>
                <label for="Estrellas" style="color: black">Estrellas</label>
                <select class="form-control" name="Estrellas" id="Estrellas">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <hr>
                <label for="Categoria" style="color: black">Categoria</label>
                <select class="form-control" name="Categoria" id="Categoria">
                    <option value="Fantasia">Fantasia</option>
                    <option value="Ciencia Ficcion">Ciencia Ficcion</option>
                    <option value="Comics">Comics</option>
                </select>
                <hr>
                <button class="btn btn-primary btn-block" type="button" style="background-color: black; border: black;" id="btn-send" onclick="botonActivo()">Agregar</button>
                <hr>
            </div>

        </form>

    </section>


</body>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<script src="Site/js/scripts.js"></script>

<script>

window.onload = function abrirPagina() {
	var URLsearch = window.location.search;
	if (URLsearch ==='') {
		$("#btn-send").prop('disabled',true);
	}
}

function validarCampos() {
	
	if($("#Nombre").val()==='' || $("#Autor").val()==='' || $("#CantidadDePaginas").val()==='' || $("#Portada").val()==='' || isNaN($("#CantidadDePaginas").val())) {
		$("#btn-send").prop('disabled',true);
	} else {
		$("#btn-send").prop('disabled',false);
	}
}

function validarNombre() {
	validarCampos();
}

function validarAutor() {
	validarCampos();
}

function validarCantidadDePaginas() {
	validarCampos();
}

function validarPortada() {
	validarCampos();
}

function botonActivo(){
$("#formularioID").submit();
}

</script>

</html>