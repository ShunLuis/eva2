<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Odisea</title>
</head>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

<style>

    .botones{
        background-color: black;
        border: black;
    }
    
    .border{
       border: 1px solid #000 !important;   
       min-height:20px;
       margin-bottom: 20px;
    }
  
    .col-centered{
        float: none;
        margin: 0 auto;
    }

</style>

<body style="background-color:darkgrey; color:white">

    <header class="container-fluid">
        <div class="row">
            <div class="col-3 text-center">
                <img src="https://img.icons8.com/ios/452/greek-pillar.png" alt="" class="img-fluid">
            </div>

            <div class="col-6 text-center">
                <div class="row">
                    <div class="col-12">
                        <h1 style="color: black;">ODISEA</h1>
                        <small style="color: black;">Compra de Libros Online</small>
                        <hr>
                    </div>
                    <div class="col-9 col-centered">
                        <img src="https://pbs.twimg.com/profile_images/1638041354/CASCOcuadradopng_400x400.png" alt="" class="img-fluid" width="250" >
                    <hr>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-primary btn-block" href="HomeControllers.do" style="background-color: black; border: black;">Inicio</a>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-primary btn-block" href="LibrosControllers.do" style="background-color: black; border: black;">Libros</a>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-primary btn-block" href="LoginControllers.do" style="background-color: black; border: black;">Administracion</a>
                    </div>
                </div>
            </div>

            <div class="col-3 text-center">
                <img src="https://img.icons8.com/ios/452/greek-pillar.png" alt="" class="img-fluid">
            </div>
        </div>
        
        <div class="col-md-12" style="background-color: black;"><hr></div>
        
    </header>