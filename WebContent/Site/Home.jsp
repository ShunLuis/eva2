<jsp:include page="header.jsp"/>

<%@page import="java.util.List" %>
<%@page import="cl.Inacap.Odiseo.DAO.LibroDAO" %> 
<%@page import="cl.Inacap.Odiseo.DTO.Libro"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
		
		
		<div class="col-12 text-center" style="color: black;">Solo libros Destacados aqu�:</div>
		<div class="col-md-12" style="background-color: black;"><hr></div>

	    <section class="container-fluid">

        <div class="row">
        
        	<c:forEach items="${Libros }"  var="l" varStatus="recorrido"> 
            
            <div class="col-4 col-centered text-center">
            	<a href="DetalleLibroControllers.do?index=${recorrido.index}">
            	<img src="${l.portadaLibro }" alt="">
            	</a>
                <h1>${l.nombreLibro }</h1>
            </div>
            
			</c:forEach>
        </div>

    </section>

</body>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</html>