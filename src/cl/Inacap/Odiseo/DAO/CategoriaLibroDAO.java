package cl.Inacap.Odiseo.DAO;

import java.util.ArrayList;
import java.util.List;

import cl.Inacap.Odiseo.DTO.CategoriaLibro;

public class CategoriaLibroDAO {
	
	private static List<CategoriaLibro> categoriasLibros = new ArrayList<CategoriaLibro>();
	
	public void addCategoriaLibro() {
		
		categoriasLibros.clear();
		
		CategoriaLibro fantasia = new CategoriaLibro();
		fantasia.setIdCategoria(1);
		fantasia.setCategoriaLibro("Fantasia");
		categoriasLibros.add(fantasia);
		
		CategoriaLibro cienciaFiccion = new CategoriaLibro();
		cienciaFiccion.setIdCategoria(2);
		cienciaFiccion.setCategoriaLibro("Ciencia Ficcion");
		categoriasLibros.add(cienciaFiccion);
		
		CategoriaLibro comics = new CategoriaLibro();
		comics.setIdCategoria(3);
		comics.setCategoriaLibro("Comics");
		categoriasLibros.add(comics);
		
	}
	
	public List<CategoriaLibro> getCategoriaLibro() {
		return categoriasLibros;
	}
	

}
