package cl.Inacap.Odiseo.DAO;

import java.io.BufferedReader; //Leer Datos de Libros.txt
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Files;    //Leer Datos de Libros.txt
import java.nio.file.Path;     //Leer Datos de Libros.txt
import java.nio.file.Paths;    //Leer Datos de Libros.txt
import java.util.ArrayList;
import java.util.List;

import cl.Inacap.Odiseo.DTO.CategoriaLibro;
import cl.Inacap.Odiseo.DTO.Libro;

//////////MENSAJE PARA EL PROFE: No alcanc� a hacer el pendientes, perdoneme//////////
//////////Pendiente: Pasar todo lo que tiene que ver con CategoriaLibro a CategoriaLibroDAO//////////

public class LibroDAO {
	
	private static List<Libro> libros = new ArrayList<Libro>();
	private static List<CategoriaLibro> categoriasLibros = new ArrayList<CategoriaLibro>();
	
	//Leer Datos de Personas.txt
	public static void leerTxt(boolean destacado) {
		Path archivoTxt = Paths.get("C:\\Users\\Shun\\eclipse-workspace\\Odiseo\\src\\cl\\Inacap\\Odiseo\\DAO\\Libros.txt");
		try {
			BufferedReader br = Files.newBufferedReader(archivoTxt);
			String linea;
			while((linea = br.readLine()) !=null) {
				if (destacado) {
					String[] datosDeLinea = linea.split(",");
					Libro libro = new Libro();
					libro.setIdLibro(Integer.parseInt(datosDeLinea[0]));
					libro.setNombreLibro(datosDeLinea[1]);
					libro.setAutorLibro(datosDeLinea[2]);
					libro.setCantPaginas(Integer.parseInt(datosDeLinea[3]));
					libro.setBooleanDestacado(Boolean.parseBoolean(datosDeLinea[4]));
					libro.setPortadaLibro(datosDeLinea[5]);
					libro.setEstrellas(Integer.parseInt(datosDeLinea[6]));
					if (datosDeLinea[7].equals("Fantasia")) {
						libro.setCategoria(categoriasLibros.get(0));
					} else if (datosDeLinea[7].equals("Ciencia Ficcion")) {
						libro.setCategoria(categoriasLibros.get(1));
					} else if (datosDeLinea[7].equals("Comics")) {
						libro.setCategoria(categoriasLibros.get(2));
					}
					libros.add(libro);
				} else {
					String[] datosDeLinea = linea.split(",");
					if (datosDeLinea[4].equals("true")) {
						Libro libro = new Libro();
						libro.setIdLibro(Integer.parseInt(datosDeLinea[0]));
						libro.setNombreLibro(datosDeLinea[1]);
						libro.setAutorLibro(datosDeLinea[2]);
						libro.setCantPaginas(Integer.parseInt(datosDeLinea[3]));
						libro.setBooleanDestacado(Boolean.parseBoolean(datosDeLinea[4]));
						libro.setPortadaLibro(datosDeLinea[5]);
						libro.setEstrellas(Integer.parseInt(datosDeLinea[6]));
						if (datosDeLinea[7].equals("Fantasia")) {
							libro.setCategoria(categoriasLibros.get(0));
						} else if (datosDeLinea[7].equals("Ciencia Ficcion")) {
							libro.setCategoria(categoriasLibros.get(1));
						} else if (datosDeLinea[7].equals("Comics")) {
							libro.setCategoria(categoriasLibros.get(2));
						}
						libros.add(libro);
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Escribir Datos en Personas.txt
	public static void escribirTxt() {
		File archivoTxt = new File("C:\\Users\\Shun\\eclipse-workspace\\Odiseo\\src\\cl\\Inacap\\Odiseo\\DAO\\Libros.txt");
		try {
			FileWriter w = new FileWriter(archivoTxt);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);
			for (int i = 0; i < libros.size(); ++i) {
				Libro linea = libros.get(i);
				String lineaTxt = (linea+"\n");
				wr.append(lineaTxt);
			}
			wr.close();
			bw.close();
		} catch (Exception e) {
		}
	}
	
	public void crearCategoria() {
		
		categoriasLibros.clear();
		
		CategoriaLibro fantasia = new CategoriaLibro();
		fantasia.setIdCategoria(1);
		fantasia.setCategoriaLibro("Fantasia");
		categoriasLibros.add(fantasia);
		
		CategoriaLibro cienciaFiccion = new CategoriaLibro();
		cienciaFiccion.setIdCategoria(2);
		cienciaFiccion.setCategoriaLibro("Ciencia Ficcion");
		categoriasLibros.add(cienciaFiccion);
		
		CategoriaLibro comics = new CategoriaLibro();
		comics.setIdCategoria(3);
		comics.setCategoriaLibro("Comics");
		categoriasLibros.add(comics);
		
	}
	
	//Agregar Libro
	public void addLibro(Libro libro, String categoria) {
		if (categoria.equals("Fantasia")) {
			libro.setCategoria(categoriasLibros.get(0));
		} else if (categoria.equals("Ciencia Ficcion")) {
			libro.setCategoria(categoriasLibros.get(1));
		} else if (categoria.equals("Comics")) {
			libro.setCategoria(categoriasLibros.get(2));
		}
		libro.setIdLibro(libros.size()+1);
		libros.add(libro);
		escribirTxt();
	}
	
	//Listar Libros
	public List<Libro> getLibros(boolean destacado) {
		//new CategoriaLibroDAO()
		crearCategoria();
		libros.clear();
		leerTxt(destacado);
		return libros;
	}
	
	//Seleccionar Libro
	public Libro selectLibro(int id) {
		return libros.get(id);
	}
	
	//Actualizar Libro
	public void uptadeLibro(Libro libro, int id, String categoria) {
		if (categoria.equals("Fantasia")) {
			libro.setCategoria(categoriasLibros.get(0));
		} else if (categoria.equals("Ciencia Ficcion")) {
			libro.setCategoria(categoriasLibros.get(1));
		} else if (categoria.equals("Comics")) {
			libro.setCategoria(categoriasLibros.get(2));
		}
		libro.setIdLibro(id);
		libros.set(id, libro);
		escribirTxt();
	}
	
	//Eliminar Libro
	public void deleteLibro(int id) {
		libros.remove(id);
		escribirTxt();
	}

}
